#!/usr/bin/python3
import sys

try:
    from .__id__ import APP_NAME
    from .backend import UserDirs, PALETTE_DEFAULT, STYLE_DEFAULT
except (ValueError, ImportError):
    from __id__ import APP_NAME
    from backend import UserDirs, PALETTE_DEFAULT, STYLE_DEFAULT

warning = (
    f"This file is automatically generated, all changes will be lost. Close {APP_NAME} before editing."
)

CSS_DEFAULT = {
    "border-color": "#000000",
    "context-menu": "true",
    "encryption-icon": "#8e8e8e",
    "encryption-icon-active": "#8e8e8e",
    "highlight-bg": "#ffffa6",
    "highlight-fg": "#000000",
    "menu-icon": "#404040",
    "msgbar-visible": "true",
    "popup-visible": "true",
    "preview-bg": "#ffffff",
    "preview-fg": "#000000",
    "status-icon": "#bfbfbf",
    "status-icon-active": "#a8a8a8",
    "toolbar-align": "center",
    "toolbar-icon": "#bfbfbf",
    "toolbar-icon-active": "#bfbfbf",
    "toolbar-visible": "true",
    "tray-icon": "none",
}

NDB_DEFAULT = {"warning": warning, "favorites": [], "loaded": []}

SDB_DEFAULT = {
    "warning": warning,
    "general": {
        "default name": "Untitled",
        "skip taskbar": True,
        "hotkeys": True,
        "minimize": False,
        "reposition": True,
        "accept qrichtext": True,
        "encrypt all": False,
        "file monitor": False,
        "elide threshold": 60,
        "wheel threshold": 10,
        "truncate threshold": 35,
        "maximum load": 20,
        "ignored css": ["debug.css"],
    },
    "archives": {
        "frequency": 1,
        "image": True,
        "text": True,
    },
    "clean": {
        "archives": 90,
        "blanks": True,
        "logs": 7,
        "trash": 31,
    },
    "terminal": {
        "color scheme": "default.colorscheme",
        "font family": "DejaVu Sans Mono",
        "font size": 9,
    },
    "message bar": {
        "encryption": True,
        "folder": True,
        "pixels": True,
        "words": True,
        "popup interval": 1500,
    },
    "launcher": {
        "case": False,
        "hide": True,
    },
    "runtime": {
        "export directory": str(UserDirs.NOTES),
    },
    "profile default": {
        "position": [0, 0],
        "width": 300,
        "height": 210,
        "opacity": 1.0,
        "zoom": 0,
        "pin": False,
        "wrap": True,
        "style": STYLE_DEFAULT,
        "palette": PALETTE_DEFAULT,
        "mode": "html",
    },
    "key events": {
        "ctrl;a": ["note", "select all"],
        "ctrl;b": ["note", "bold"],
        "ctrl;c": ["note", "copy"],
        "ctrl;d": ["note", "line duplicate"],
        "ctrl;f": ["note", "search"],
        "ctrl;h": ["note", "highlight"],
        "ctrl;i": ["note", "italic"],
        "ctrl;m": ["note", "move"],
        "ctrl;p": ["note", "pin"],
        "ctrl;q": ["note", "unload"],
        "ctrl;r": ["note", "rename"],
        "ctrl;s": ["note", "strike"],
        "ctrl;u": ["note", "underline"],
        "ctrl;v": ["note", "paste"],
        "ctrl;w": ["note", "wrap"],
        "ctrl;x": ["note", "cut"],
        "ctrl;y": ["note", "redo"],
        "ctrl;z": ["note", "undo"],
        "ctrl;-": ["note", "zoom out"],
        "ctrl;=": ["note", "zoom in"],
        "ctrl,shift;backspace": ["note", "line delete"],
        "ctrl,shift;up": ["note", "line up"],
        "ctrl,shift;down": ["note", "line down"],
        "ctrl,shift;a": ["note", "select"],
        "ctrl,shift;c": ["note", "copy rich"],
        "ctrl,shift;l": ["note", "lowercase"],
        "ctrl,shift;p": ["note", "antidote corrector"],
        "ctrl,shift;r": ["note", "reset"],
        "ctrl,shift;s": ["note", "sort"],
        "ctrl,shift;t": ["note", "titlecase"],
        "ctrl,shift;u": ["note", "uppercase"],
        "ctrl,shift;v": ["note", "special paste"],
        "ctrl,shift;w": ["note", "clear format"],
        "ctrl,shift;x": ["note", "cut rich"],
    },
    "mouse events": {
        "tray": {
            "left": ["core", "toggle"],
            "middle": ["core", "new"],
        },
        "status": {
            "left": ["note", "swap"],
            "middle": ["core", "new"],
            "right": ["core", "menu"],
            "doubleclick": [],
            "up": ["note", "opacity+"],
            "down": ["note", "opacity-"],
        },
        "title": {
            "middle": ["note", "resize"],
            "right": [],
            "doubleclick": ["note", "rename"],
            "up": ["note", "roll"],
            "down": ["note", "unroll"],
        },
        "close": {
            "left": ["note", "unload"],
            "middle": [],
            "right": [],
            "doubleclick": [],
            "up": [],
            "down": [],
        },
    },
    "core menus": {
        "tray": [
            "new",
            "reset",
            "search",
            "open",
            "separator",
            "unload",
            "folders list",
            "separator",
            "notes list",
            "separator",
            "settings",
            "quit",
        ],
        "browser": [
            "separator",
            "new",
            "show",
            "load",
            "unload",
            "separator",
            "rename",
            "move",
            "open",
            "separator",
            "delete",
        ],
    },
    "context menus": {
        "html": [
            "new",
            "duplicate",
            "move",
            "export",
            "separator",
            "antidote corrector",
            "cut",
            "copy",
            "paste",
            "separator",
            "palette",
            "style",
            "encryption",
            "mode",
            "separator",
            "delete",
        ],
        "image": [
            "original",
            "scale",
            "aspect ratio",
            "fit",
            "separator",
            "duplicate",
            "move",
            "export",
            "separator",
            "palette",
            "style",
            "separator",
            "delete",
        ],
        "plain": [
            "new",
            "duplicate",
            "move",
            "export",
            "separator",
            "antidote corrector",
            "cut",
            "copy",
            "paste",
            "separator",
            "palette",
            "style",
            "encryption",
            "mode",
            "separator",
            "delete",
        ],
        "console": [
            "new",
            "duplicate",
            "move",
            "reload",
            "separator",
            "zoom in",
            "zoom out",
            "separator",
            "palette",
            "style",
            "mode",
            "separator",
            "delete",
        ],
        "vim": [
            "new",
            "duplicate",
            "move",
            "export",
            "separator",
            "zoom in",
            "zoom out",
            "separator",
            "palette",
            "style",
            "mode",
            "separator",
            "delete",
        ],
    },
    "toolbar menus": {
        "html": [
            "bold",
            "italic",
            "underline",
            "strike",
            "highlight",
            "clear format",
            "separator",
            "duplicate",
            "move",
            "lock|unlock",
            "separator",
            "delete",
        ],
        "image": [
            "original",
            "scale",
            "aspect ratio",
            "fit",
            "separator",
            "duplicate",
            "move",
            "palette",
            "style",
            "separator",
            "delete",
        ],
        "plain": [
            "new",
            "duplicate",
            "move",
            "lock|unlock",
            "separator",
            "antidote corrector",
            "palette",
            "style",
            "encryption",
            "mode",
            "separator",
            "delete",
        ],
        "console": [
            "new",
            "duplicate",
            "move",
            "reload",
            "separator",
            "zoom in",
            "zoom out",
            "separator",
            "palette",
            "style",
            "mode",
            "separator",
            "delete",
        ],
        "vim": [
            "new",
            "duplicate",
            "move",
            "separator",
            "zoom in",
            "zoom out",
            "separator",
            "palette",
            "style",
            "mode",
            "separator",
            "delete",
        ],
    },
    "legacy": 0.0,
}

if sys.platform.startswith("win"):
    SDB_DEFAULT["key events"]["ctrl;_"] = ["note", "zoom out"]
