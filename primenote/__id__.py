VERSION = 1.5
DATE = "2024/10/10"

APP_NAME = "PrimeNote"
ID = APP_NAME.lower()
DESC_AUTHOR = "© William Belanger, licensed under GPLv3"
DESC_SHORT = f"{APP_NAME} is a free and open source note-taking tool."
DESC_LONG = f"{APP_NAME} is a clever and highly adaptable note-editing tool designed for both Linux and Windows. Its functionality allows for swift editing, storage, organization, and backup of an unlimited number of notes. With {APP_NAME}, users can take advantage of advanced text operations and an extensive range of modes encompassing text and image processing, as well as a built-in terminal emulator and Vim integration for advanced users."

LINK_AUR = "https://aur.archlinux.org/packages/primenote-git"
LINK_DONATION = "https://www.paypal.com/donate?hosted_button_id=7UTK3HPH6Q5DG"
LINK_HOMEPAGE = "https://www.primenote.cc"
LINK_SOURCE = "https://www.gitlab.com/william.belanger/primenote"

HELP = [
    "\033[1m--help, -h:\033[0m Show this help message",
    "\033[1m--reset, -r <option>:\033[0m Reset configuration files to default",
    "\tOptions: {all,console,css,profiles,settings,vim}",
    "\tDefault: {all}\n",
    "\033[1m--search, -s <option>:\033[0m Toggle, show or hide the note search utility",
    "\tOptions: {toggle,show,hide}",
    "\tDefault: {toggle}\n",
    "\033[1m--verbose, -v <option>:\033[0m Set console logging level",
    "\tOptions: {debug,info,warning,error,critical,quiet}",
    "",
    "\033[1mCORE ACTIONS\033[0m",
    "\033[1m--core, -c <action>\033[0m",
    "  hide\tHide all notes",
    "  load\tLoad all notes in nested folders",
    "  new\tCreate a new note",
    "  menu\tPopup main menu at mouse position",
    "  open\tOpen notes and settings folder in file manager\t",
    "  quit\tClose the application",
    "  reset\tShow all and reset profiles",
    "  reverse\tToggle visibility of loaded notes",
    "  search\tToggle the note search utility",
    "  settings\tOpen application settings",
    "  show\tShow all notes",
    "  toggle\tToggle visibility of favorites notes",
    "  unload\tUnload all notes in nested folders",
    "  wizard\tOpen the setup wizard",
    "",
    "\033[1mNOTE ACTIONS\033[0m",
    "\033[1m--note, -n <action> --path, -p <path>\033[0m",
    "  activate\tRaises and focus the window (void ::activateWindow)",
    "  delete\tDelete note",
    "  duplicate\tDuplicate note",
    "  export\tOpen an export browser dialog",
    "  hide\tHide note (void ::hide)",
    "  move\tMove note in a new folder",
    "  opacity+\tIncrease window opacity",
    "  opacity-\tDecrease window opacity",
    "  open\tOpen note directory in file manager",
    "  pin\tToggle pin status",
    "  raise\tRaises the window (void ::raise)",
    "  refresh\tUpdate the window icons and stylesheets",
    "  rename\tOpen rename dialog",
    "  roll\tMinimize screen space used, leaving only the titlebar",
    "  show\tShow note",
    "  unload\tUnload a note if nested, hide if at root",
    "  unload\tUnload nested note or hide root note",
    "  unroll\tRestore a note to its full size",
    "",
    "\033[1mNOTE ACTIONS – TEXT MODE\033[0m",
    "  copy\tCopy plain text to clipboard",
    "  cut\tDelete selection and copy plain text to clipboard",
    "  line delete\tDelete selection with its linebreak",
    "  line down\tShift line position down",
    "  line duplicate\tDuplicate current line",
    "  line end\tMove cursor position at end of the line",
    "  line start\tMove cursor position at beginning of the line",
    "  line up\tShift line position up",
    "  lock\tSet file permissions to read-only",
    "  lock|unlock\tToggle read-only mode",
    "  lowercase\tSet selection to lowercase",
    "  paste\tInsert clipboard content",
    "  redo\tRedo the last operation (void ::redo)",
    "  search\tOpen the find and replace utility",
    "  select all\tSelect all text (void ::selectAll)",
    "  shuffle\tShuffle selection",
    "  save\tSave changes to file",
    "  sort\tSort selection",
    "  special paste\tPaste text while replacing linebreaks",
    "  unlock\tSet file permissions to read-write",
    "  undo\tUndo the last operation (void ::undo)",
    "  uppercase\tSet selection to uppercase",
    "  wrap\tToggle word-wrap",
    "",
    "\033[1mNOTE ACTIONS – RICH TEXT MODE\033[0m",
    "  bold\tToggle bold decoration on the anchor or the selected text",
    "  clear format\tClear all text formatting",
    "  copy rich\tCopy formatted text to clipboard",
    "  cut rich\tDelete selection and copy formatted text to clipboard",
    "  highlight\tToggle highlight decoration on the anchor or the selected text",
    "  italic\tToggle italic decoration on the anchor or the selected text",
    "  strike\tToggle strikethrough decoration on the anchor or the selected text",
    "  underline\tToggle underline decoration on the anchor or the selected text",
    "",
    "\033[1mNOTE ACTIONS – IMAGE MODE\033[0m",
    "  original\tRestore original image size",
    "  aspect ratio\tPreserve aspect ratio while resizing",
    "  scale\tScale the image to preserve its aspect ratio",
    "",
    "\033[1mNOTE ACTIONS – CHECKLIST MODE\033[0m",
    "  save\tSave changes to file",
    "  select all\tCheck all checkbox",
    "  unselect all\tUncheck all checkbox",
]
